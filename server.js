var express = require('express');
var app = express();
var router = express.Router();
var jwt = require('jsonwebtoken');
var mongoose = require('mongoose');

var User = require('./_models/user');

var config = require('./config');
var db = require('./config/db');

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var morgan = require('morgan');
app.use(morgan('combined', {
  skip: function (req, res) { return res.statusCode < 400 }
}));

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Content, Accept, x-ozact-token");
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
  next();
});

app.use('/public', express.static('public'));

mongoose.connect(db.url);

router.post('/authenticate', function (req, res) {
  User.findOne({
    email: req.body.email
  }, function (error, user) {
    if (error) throw error;

    if (!user) {
      return res.status(403).send({ success: false, message: 'Authentication failed. User not found' });
    } else {
      user.comparePassword(req.body.password, function (error, isMatch) {
        if (isMatch && !error) {
          var token = jwt.sign(user.toObject(), config.jwtSecret);

          return res.send({
            success: true,
            message: 'Authentication successful',
            token: token,
            user: { firstname: user.firstname, lastname: user.lastname, _id: user._id, accepted_tos: user.accepted_tos, role: user.role }
          });
        } else {
          return res.status(403).send({ success: false, message: 'Authentication failed. Wrong password' });
        }
      });
    }
  });
});

require('./_routes/users')(app, router);
require('./_routes/me')(app, router);
require('./_routes/actions')(app, router);
require('./_routes/folders')(app, router);
require('./_routes/partners')(app, router);
require('./_routes/themes')(app, router);
require('./_routes/industries')(app, router);
require('./_routes/effects')(app, router);
require('./_routes/iso26k')(app, router);
require('./_routes/partner_types')(app, router);
require('./_routes/admin')(app, router);
require('./_routes/import')(app, router);

app.use('/api', router);

app.listen(3000, function () {
  console.log('Listening on port 3000');
});
