var Sequelize = require('sequelize');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var passport = require('passport')
, LocalStrategy = require('passport-local').Strategy;
var session = require('express-session');
var jwt = require('jsonwebtoken');
var config = require('./config');
multiparty = require('connect-multiparty');
multipartyMiddleware = multiparty();
const fs = require('fs');
const readline = require('readline');

var sequelize = new Sequelize('ozactShare', 'root', '', {
    host: "127.0.0.1",
    port: 8889,
    dialect: 'mysql',
    dialectOptions: {
        socketPath: '/var/run/mysqld/mysqld.sock'
    },
    define: {
        timestamps: false,
        freezeTableName: true
    },
    omitNull: true
}); 

var Action  = sequelize.import('./models/action.js');
var ActionHasIndicateur = sequelize.import('./models/action_has_indicateur.js');
var Indicateur = sequelize.import('./models/indicateur.js');
var ActionHasTagAutre = sequelize.import('./models/action_has_tagAutre.js');
var ActionHasTagSecteur = sequelize.import('./models/action_has_tagSecteur.js');
var ActionHasTagTheme = sequelize.import('./models/action_has_tagTheme.js');
var IndicateurHasUnite = sequelize.import('./models/indicateur_has_unite.js');
var Pack = sequelize.import('./models/pack.js');
var PackHasAction = sequelize.import('./models/pack_has_action.js');
var TagAutre = sequelize.import('./models/tagAutre.js');
var TagSecteur = sequelize.import('./models/tagSecteur.js');
var TagTheme = sequelize.import('./models/tagTheme.js');
var TypeAction = sequelize.import('./models/typeAction.js');
var Unite = sequelize.import('./models/unite.js');
var UniteAffichee = sequelize.import('./models/uniteAffichee.js');
var UniteHasUniteAffichee = sequelize.import('./models/unite_has_uniteAffichee.js');
var User = sequelize.import('./models/user.js');

Pack.belongsToMany(Action, { through: 'pack_has_action', foreignKey: 'pack_id', otherKey: 'action_id'});
Action.belongsToMany(Pack, { through: 'pack_has_action', foreignKey: 'action_id', otherKey: 'pack_id'});
Action.belongsToMany(Indicateur, { through: 'action_has_indicateur', foreignKey: 'action_id', otherKey: 'indicateur_id'});
Action.hasOne(TypeAction, { foreignKey: 'id'});
Action.hasOne(User, { foreignKey: 'id'});
Indicateur.belongsToMany(Action, { through: 'action_has_indicateur', foreignKey: 'indicateur_id', otherKey: 'action_id'});
Indicateur.belongsToMany(Unite, { through: 'indicateur_has_unite', foreignKey: 'indicateur_id', otherKey: 'unite_id'});
Action.belongsToMany(TagAutre, { through: 'action_has_tagAutre', foreignKey: 'action_id', otherKey: 'tagAutre_id'});
Action.belongsToMany(TagSecteur, { through: 'action_has_tagSecteur', foreignKey: 'action_id', otherKey: 'tagSecteur_id'});
Action.belongsToMany(TagTheme, { through: 'action_has_tagTheme', foreignKey: 'action_id', otherKey: 'tagTheme_id'});
TagAutre.belongsToMany(Action, { through: 'action_has_tagAutre', foreignKey: 'tagAutre_id', otherKey: 'action_id'});
TagSecteur.belongsToMany(Action, { through: 'action_has_tagSecteur', foreignKey: 'tagSecteur_id', otherKey: 'action_id'});
TagTheme.belongsToMany(Action, { through: 'action_has_tagTheme', foreignKey: 'tagTheme_id', otherKey: 'action_id'});
Unite.belongsToMany(Indicateur, { through: 'indicateur_has_unite', foreignKey: 'unite_id', otherKey: 'indicateur_id'});
Unite.belongsToMany(UniteAffichee, { through: 'unite_has_uniteAffichee', foreignKey: 'unite_id', otherKey: 'uniteAffichee_id'});
UniteAffichee.belongsToMany(Unite, { through: 'unite_has_uniteAffichee', foreignKey: 'uniteAffichee_id', otherKey: 'unite_id'});

var express = require('express');
var app = express();

app.use(morgan('combined', {
    skip: function(req, res) { return res.statusCode < 400 }
}));

app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
var sess = {
    secret: 'keyboard cat',
    cookie: {}
}

if (app.get('env') === 'production') {
    app.set('trust proxy', 1) // trust first proxy 
    sess.cookie.secure = true // serve secure cookies 
}

app.use(session(sess));
app.use(passport.initialize());
app.use(passport.session());

passport.use(new LocalStrategy(
    function(username, password, done) {
        User.findOne({ where: { email : username } }).then(function (user) {
            if (!user) {
                return done(null, false, { message: 'Incorrect username.' });
            }
            if (!validPassword(user, password)) {
                return done(null, false, { message: 'Incorrect password.' });
            }
            delete user.password;
            return done(null, user);
        });
    }
));

passport.serializeUser(function(user, done) {
    delete user.password;
    done(null, user);
});

passport.deserializeUser(function(user, done) {
    User.findById(user.id).then(function(user) {
        delete user.password;
        done(null, user);
    }).catch(function (err) {
        done(err, null);
    });;
});

function validPassword(user_, password_) {
    return user_.password == password_;
}

var checkJWT = function(req, res, next) {
    var token = req.body.token || req.query.token || req.headers['x-ozact-token'];

    if (token) {
        jwt.verify(token, config.jwtSecret, function(error, decoded) {
            if (error) {
                return res.json({ success: false, message: 'Failed to authenticate token' });
            } else {
                req.decoded = decoded;
                console.log(decoded);
                next();
            }
        })
    } else {
        return res.status(403).send({ success: false, message: 'No token provided' });
    }
};

var checkAdmin = function(req, res, next) {
    var user = req.decoded;

    if (user) {
        if (user.admin) {
            next();
        } else {
            res.status(403).send({
                success: false, 
                message: 'Admin privileges are required to do that, please contact Ozact if you think you should have admin privileges'
            });
        }
    } else {
        res.status(403).send({ success: false, message: 'Failed to find user in request' });
    }
};

var router = express.Router();

router.post('/login', passport.authenticate('local'), function(req, res){
    res.send({ message: 'Authentication successfull', role:req.session.passport.user.role, lastName:req.session.passport.user.lastName, firstName : req.session.passport.user.firstName, id: req.session.passport.user.id });
});

router.post('/authenticate', function(req, res) {
    User.findOne({ where: { email : req.body.username } }).then(function (user) {
        console.log(user.dataValues);
        if (!user) {
            res.json({ success: false, message: 'User does not exist' });
        }
        if (!validPassword(user, req.body.password)) {
            res.json({ success: false, message: 'Incorrect password' });
        }

        var token = jwt.sign(user.dataValues, config.jwtSecret);
        res.json({
            success: true,
            message: 'Authentication successful',
            token: token
        });
    });
})

router.get('/logout', function(req, res){
    req.logout();
    res.send({message:'Vous êtes déconnecté'});
});

router.get('/loggedin', function(req, res) { res.send(req.isAuthenticated() ? req.user : '0'); });

router.get('/', function (req, res) {
    res.send('Hello World!');
});

router.get('/actions', checkJWT, function (req, res) {
    if(req.query.query) {
        Action.findAll({
            include:[TagTheme],
            where: ['MATCH(titre, description) AGAINST(?)', req.query.query]
        }).then(function (actions) {
            var jsonOutput = JSON.stringify(actions);
            res.send(jsonOutput);
        })
    } else {
        Action.findAll({include:[TagTheme]}).then(function (actions) {
            var jsonOutput = JSON.stringify(actions);
            res.send(jsonOutput);
        });
    }
});

router.get('/action/:id', checkJWT, function (req, res) {
    var actionId = req.params.id;
    if(actionId) {
        Action.findOne({
            where: {
                id: actionId
            },
            include: [{ all: true, nested: true, required: false }]
        }).then(function (action) {
            var jsonOutput = JSON.stringify(action);
            res.send(jsonOutput);    
        })
    } else {
        res.send(400);
    };
});


router.get('/indicateurs', checkJWT, function (req, res) {
    if(req.query.search) {
        Indicateur.findAll({
            where: ['MATCH(titre, description) AGAINST(?)', req.query.search]
        }).then(function (indicateurs) {
            var jsonOutput = JSON.stringify(indicateurs);
            res.send(jsonOutput);    
        })
    } else {
        Indicateur.findAll({include: [Unite]}).then(function (indicateurs) {
            var jsonOutput = JSON.stringify(indicateurs);
            res.send(jsonOutput);
        });
    }
});

router.get('/unites', checkJWT, function (req, res) {
    if(req.query.search) {
        Unite.findAll({
            where: ['MATCH(titre, description) AGAINST(?)', req.query.search]
        }).then(function (unites) {
            var jsonOutput = JSON.stringify(unites);
            res.send(jsonOutput);    
        })
    } else {
        Unite.findAll({include: [UniteRattachee], required:false}).then(function (unites) {
            var jsonOutput = JSON.stringify(unites);
            res.send(jsonOutput);
        });
    }
});

router.get('/packs', checkJWT, function (req, res) {
    if(req.query.search) {
        Pack.findAll({
            where: ['MATCH(titre, description2) AGAINST(?)', req.query.search]
        }).then(function (actions) {
            var jsonOutput = JSON.stringify(actions);
            res.send(jsonOutput);    
        })
    } else {
        Pack.findAll({ include: [{ all: true, nested: true, required: false }]}).then(function (packs) {
            var jsonOutput = JSON.stringify(packs);
            res.send(jsonOutput);
        });
    }
});

router.get('/tagThemes', checkJWT, function (req, res) {
    TagTheme.findAll().then(function (packs) {
        var jsonOutput = JSON.stringify(packs);
        res.send(jsonOutput);
    });
});

router.get('/users', checkJWT, function (req, res) {
    User.findAll({attributes: ['id', 'lastName', 'firstName', 'organisation', 'email']}).then(function (users) {
        var jsonOutput = JSON.stringify(users);
        res.send(jsonOutput);
    });
});

router.get('/pack/:id', checkJWT, function (req, res) {
    var packId = req.params.id;
    if(packId) {
        Pack.findAll({
            where: {
                id: packId
            }
        }).then(function (packs) {
            var jsonOutput = JSON.stringify(packs);
            res.send(jsonOutput);    
        })
    } else {
        Pack.findAll().then(function (packs) {
            var jsonOutput = JSON.stringify(packs);
            res.send(jsonOutput);
        });
    }
});

router.get('/pack/:id/actions', checkJWT, function (req, res) {
    var packId = req.params.id;
    if(packId) {
        Action.findAll({
            where: {
                idPack: packId
            }
        }).then(function (packs) {
            var jsonOutput = JSON.stringify(packs);
            res.send(jsonOutput);    
        })
    }
});

router.post('/user', checkJWT, function(req, res) {
    User.create(req.body).then(function(createdUser) {
        res.send(createdUser); 
    });
});

router.post('/action/:id/delete', checkJWT, function(req, res) {
    var userId = req.params.id;
    Action.destroy({
        where: {
            id: userId //this will be your id that you want to delete
        }
    }).then(function(rowDeleted){
        res.send({message:'action destroyed'});
    });
});

router.post('/user/:id/delete', checkJWT, function(req, res) {
    var userId = req.params.id;
    User.destroy({
        where: {
            id: userId //this will be your id that you want to delete
        }
    }).then(function(rowDeleted){
        res.send({message:'user destroyed'});
    });
});

router.post('/action', checkJWT, function (req, res) {
    Action.create(req.body, { include: [{ all: true, nested: true, required: false }]}).then(function(createdAction) {
        res.send(createdAction);
    });
});

router.post('/import', checkJWT, multipartyMiddleware, function(req, res) {
    var file = req.files.file;
    var output = [];
    const rl = readline.createInterface({
        input: fs.createReadStream(file.path)
    });
    rl.on('line', function (line) {
        console.log('Line from file:', line);
        var attributesArray = line.split('@');
        console.log(attributesArray.length);
        if(attributesArray.length !== 7) {
            output.push({line: line, message : 'not enough separator'});
        } else {
            //process line ID action@Titre@typeActionId@description@tagThemeId@userId@
            var action = {
                id: parseInt(attributesArray[0]),
                titre: attributesArray[1],
                typeActionId: parseInt(attributesArray[2]),
                description: attributesArray[3],
                theme: {id: parseInt(attributesArray[4])},
                userId:  parseInt(attributesArray[5])
            };
            Action.findById(action.id).then(function(actionInDb){
                //trouvé !
                console.log('trouvé -> mise à jour');
                console.log(actionInDb);
                if(!actionInDb) {
                    //creation de l'action avec les valeurs
                    Action.create(action, { include: [{ all: true, nested: true, required: false }]}).then(function(actionCreated) {
                        output.push({line: line, actionCreated: JSON.stringify(actionCreated), message : 'action created'});
                    });
                } else {
                    //mise à jour de l'action
                    actionInDb.updateAttributes({
                        titre: action.titre,
                        description:action.description,
                        tag:action.userId,
                        typeActionId:action.typeActionId,
                        theme: {
                            id: action.tagThemeId
                        }
                    }).then(function(actionUpdated) {
                        output.push({line: line, actionUpdated: JSON.stringify(actionUpdated), message : 'action updated'});
                    });
                }
            }).catch(function (e) {
                //non trouvé !
                console.log(e);
                res.status(500).send(e);
                throw e;
            });

            console.log(action);
            //Search for action, if found update else create
        }

    });
    rl.on('close', function(){
        res.send(JSON.stringify(output));
    });

})

app.use('/api', router);

app.listen(3000, function () {
    console.log('OzactShare is listening on port 3000!');
});