# Ozact Share Server

## How to install  
You will need:
- MongoDB | https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/
- node | `apt-get install node`
- npm
- nodemon | `npm install -g nodemon` Allows to reload server at every file-change

## How to run
- `git clone https://gitlab.com/ozact/share-server && cd share-server`
- `nodemon server.js`
- The server will be running on port 3000 by default