'use strict';

var jwt = require('jsonwebtoken');
var config = require('../config');

module.exports = {
  checkJWT: function (req, res, next) {
    var token = req.body.token || req.query.token || req.headers['x-ozact-token'];

    if (token) {
      jwt.verify(token, config.jwtSecret, function (error, decoded) {
        if (error) {
          return res.json({ success: false, message: 'Failed to authenticate token' });
        } else {
          decoded.password = '';
          req.decoded = decoded;
          next();
        }
      })
    } else {
      return res.status(403).send({ success: false, message: 'No token provided' });
    }
  },

  checkAdmin: function (req, res, next) {
    var user = req.decoded;

    if (user) {
      if (user.role === 'ADMIN') {
        next();
      } else {
        res.status(403).send({
          success: false,
          message: 'Admin privileges are required to do that, please contact Ozact if you think you should have admin privileges'
        });
      }
    } else {
      res.status(403).send({ success: false, message: 'Failed to find user in request' });
    }
  }

}