'use strict';

var authMw = require('../_mw/auth');
var multer = require('multer');
var upload = multer({ dest: 'public/img/effects' })

var Effect = require('../_models/effect');

module.exports = function (app, router) {
  router.get('/effects', authMw.checkJWT, function (req, res) {
    Effect.find().exec(function (error, effects) {
      if (error) { return res.send(error); }

      res.send(effects);
    });
  });

  router.post('/effects', authMw.checkJWT, authMw.checkAdmin, upload.any(), function(req, res) {
      var effect = new Effect({
      name: req.body.name,
      image: req.files && '/public/img/effects/' + req.files[0].filename,
      themes: req.body.themes && JSON.parse(req.body.themes),
      description: req.body.description && req.body.description
    });
    
    effect.save(function(error) {
      if (error) return res.status(555).send({ success: false, message: 'Error while creating effect', error: error });

      return res.send({ success: true, message: 'Effect has been created' });
    });
  });

  router.delete('/effects/:id', authMw.checkJWT, authMw.checkAdmin, function(req, res) {
    Effect.remove({ _id: req.params.id }, function(error) {
      if (error) { return res.status(555).send({ success: false, message: 'Error while deleting effect', error: error }); }

      return res.send({ success: true, message: 'Effect has been removed' });
    });
  });
};