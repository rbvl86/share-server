var Iso26kfoa = require('../_models/iso26kfoa');
var authMw = require('../_mw/auth');

module.exports = function(app, router) {
  router.get('/iso26kfoa', authMw.checkJWT, function(req, res) {
    Iso26kfoa.find({})
      .exec(function(error, iso26kfoas) {
        if (error) { return res.status(555).send({ success: false, message: 'Could not retreive iso26kfoas', error: error }); }

        return res.send(iso26kfoas);
      })
  })
}