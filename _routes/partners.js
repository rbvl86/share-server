'use strict';

var authMw = require('../_mw/auth');
var Partner = require('../_models/partner');
var User = require('../_models/user');
var Action = require('../_models/action');

module.exports = function(app, router) {
  router.get('/partners', function(req, res) {
    Partner.find({})
      .populate('partner_type_ids')
      .exec(function(error, partners) {
        return res.send(partners);
      });
  });

  router.route('/partners/:id')
    .get(authMw.checkJWT, function (req, res) {
      Partner.findOne({ _id: req.params.id })
        .exec(function (error, partners) {
          if (error) { return res.send(error); }

          res.send(partners);
        });
    })
    .put(authMw.checkJWT, function (req, res) {
      if (req.decoded.partner == req.params.id || req.decoded.role === 'ADMIN') {
        Partner.findOne({ _id: req.params.id })
          .exec(function (error, partner) {
            if (error) { return res.send(error); }

            req.body.name && (partner.name = req.body.name);
            req.body.logo && (partner.logo = req.body.logo);
            req.body.description && (partner.description = req.body.description);
            req.body.website && (partner.website = req.body.website);
            req.body.streetAddress && (partner.streetAddress = req.body.streetAddress);
            req.body.postcode && (partner.postcode = req.body.postcode);
            req.body.city && (partner.city = req.body.city);

            partner.save();
            return res.send({ success: true, message: 'Partner has been saved' });
          });
      } else {
        return res.status(403).send({
          success: false,
          message: 'You cannot edit an organization you\'re not part of'
        });
      }
    });
  
  router.get('/partners/:id/actions', authMw.checkJWT, function (req, res) {
    Action.find({ draft: { $in: [false, '', null, undefined] } }).populate({
      path: 'author',
      match: {
        partner: req.params.id
      }
    })
    .populate('theme')
    .exec(function (error, actions) {
      if (error) { return res.send(error); }

      actions = actions.filter(function (action) {
        return action.author;
      });

      return res.send(actions);
    });
  });

  router.get('/partners/:id/users', authMw.checkJWT, function (req, res) {
    User.find({ partner: req.params.id })
      .exec(function (error, users) {
        if (error) { return res.send(error); }

        return res.send(users);
      });
  });
};