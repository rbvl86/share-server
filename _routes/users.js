'use strict';

var multer = require('multer');
var upload = multer({ dest: 'public/img/u' });

var User = require('../_models/user');
var Action = require('../_models/action');
var Folder = require('../_models/folder');
var mw = require('../_mw/auth');


module.exports = function(app, router) {
  router.get('/users', mw.checkJWT, function(req, res) {
    User.find({})
      .populate('partner')
      .exec(function(error, users) {
        if (error) { return res.send(error); }

        return res.send(users);
      });
  });

  router.post('/users', mw.checkJWT, mw.checkAdmin, function(req, res) {
    var user = new User(req.body);

    user.save(function(error, user) {
      if (error) { return res.status(555).send({ success: false, message: 'Error while creating user', error: error }); }

      res.send({ success: true, message: 'User '+ user.firstname + ' '+ user.lastname +' sucessfully created', user: user });
    })
  })


  router.get('/users/:id', mw.checkJWT, function (req, res) {
    User.findOne({ _id: req.params.id })
      .lean()
      .populate('partner')
      .exec(function (error, user) {
      if (error)  { return res.send(error); }

      if (user) {
        user.isMe = (user._id == req.decoded._id);
        return res.send(user);
      } else {
        return res.status(555).send({ success: false, message: 'User not found' });
      }
    });
  });

  router.put('/users/:id', mw.checkJWT, upload.any(), function (req, res) {
    if (req.decoded._id !== req.params.id && req.decoded.role !== 'ADMIN') {
      return res.status(555).send({ success: false, message: 'You are not allowed to edit a user that is not yourself unless you are an admin' });
    }

    User.findOne({ _id: req.params.id }, function (error, user) {
      if (error) { return res.status(555).send(error); }
      if (!user) { return res.status(555).send({ success: false, message: 'User not found' }); }
      
      req.body.linkedin && (user.linkedin = req.body.linkedin);
      req.body.password && (user.password = req.body.password);
      req.body.bio && (user.bio = req.body.bio);
      req.body.title && (user.title = req.body.title);
      req.body.image && (user.avatar = req.body.image);
      req.files[0] && (user.avatar = '/public/img/u/'+req.files[0].filename);

      user.save(function(error) {
        if (error) { return res.status(555).send(error) }
        return res.send({ success: true, message: 'User saved' });
      });
    });
  });

  router.get('/users/:id/actions', mw.checkJWT, function(req, res) {
    Action.find({ author: req.params.id, draft: { $in: [false, '', null, undefined] } })
      .populate('theme')
      .populate('indicators')
      .populate('author')
      .exec(function(error, actions) {
        if (error) { return res.status(555).send(error); }

        return res.send(actions);
      });
  });

  router.get('/users/:id/drafts', mw.checkJWT, function(req, res) {
    Action.find({ author: req.params.id, draft: true })
      .populate('theme')
      .populate('author')
      .exec(function(error, drafts) {
        if (error) { return res.status(555).send(error); }

        if (req.decoded._id != req.params.id || req.decoded.role != 'ADMIN') {
          return res.send([]);
        }

        return res.send(drafts);
      });
  });

  router.get('/users/:id/folders', mw.checkJWT, function (req, res) {
    Folder.find({
      $or: [
        { owner: req.params.id },
        { users: req.params.id }
      ]
    })
      .populate({
        path: 'actions',
        model: 'Action',
        populate: {
          path: 'theme',
          model: 'Theme'
        }
      })
      .exec(function (error, folders) {
        if (error) { return res.send(error); }

        if (req.decoded._id != req.params.id && req.decoded.role !== 'ADMIN') {
          return res.send([]);
        }

        return res.send(folders);
      })
  });
}