'use strict';

var Action = require('../_models/action');
var User = require('../_models/user');
var Folder = require('../_models/folder');
var mw = require('../_mw/auth');

function getFolderCount(actionId) {
  return Folder.find({ actions: actionId }).count(function(error, count) {
    if (error) { return error; }

    return count;
  });
}


module.exports = function(app, router) {
  module.actions = [];
  
  router.get('/me', mw.checkJWT, function(req, res) {
    User
    .findOne({
      _id: req.decoded._id
    })
    .populate('partner')
    .lean()
    .exec(function(error, user) {
      if (error) res.send(error);

      if (user) {
        user.password = '';
        delete user.password;

        user.isMe = true;

        return res.send(user);
      } else {
        return res.send({ success: false, message: 'User not found' });
      }
    });
  });

  router.get('/me/folders', mw.checkJWT, function (req, res) {
    Folder.find({
      $or: [
        { owner: req.decoded._id },
        { users: req.decoded._id }
      ]
    })
      .populate({
        path: 'actions',
        model: 'Action',
        populate: {
          path: 'theme',
          model: 'Theme'
        }
      })
      .exec(function (error, folders) {
        if (error) { return res.send(error); }

        res.send(folders);
      })
  });

  router.get('/me/actions', mw.checkJWT, function(req, res) {
    var query = Action.find({ draft: { $in: [false, '', null, undefined] }, author: req.decoded._id });
    if (req.query.query) {
      var words = req.query.query.replace('%20', '|');
      var regexp = new RegExp(words, 'i');
      query.or([
        { title: regexp },
        { description: regexp },
        { steps: regexp },
        { stakes: regexp },
        { objective: regexp },
        { rules: regexp },
        { sources: regexp }
      ]);
    }

    query
      .lean()
      .populate({
        path: 'author',
        model: 'User',
        populate: {
          path: 'partner',
          model: 'Partner'
        }
      })
      .populate('theme')
      .exec(function (error, actions) {
        if (error) return res.send(error);
        var counts = [];
        module.actions = actions;
        
        for (var i = 0; i < actions.length; i++) {
          counts[i] = getFolderCount(actions[i]._id);
        }

        Promise.all(counts).then(function(counts) {
          for (var i = 0; i < module.actions.length; i++) {
            module.actions[i].folderCount = counts[i];
          }

          res.send(module.actions);
        });
      });
  });

  router.get('/me/drafts', mw.checkJWT, function(req, res) {
    Action.find({
      author: req.decoded._id,
      draft: true
    })
    .populate({
      path: 'author',
      model: 'User',
      populate: {
        path: 'partner',
        model: 'Partner'
      }
    })
    .populate('theme')
    .exec(function(error, drafts) {
      if (error) return res.status(555).send(error);

      return res.send(drafts);
    });
  });

  router.get('/me/accept_tos', mw.checkJWT, function(req, res) {
    User.findOne({ _id: req.decoded._id })
      .exec(function(error, user) {
        if (error) { return res.status(555).send({ success: false, message: 'Error while finding user to accept ToS', error: error }); }

        user.accepted_tos = true;

        user.save(function(error) {
          if (error) { return res.status(555).send({ success: false, message: 'Error while accepting ToS', error: error }); }

          return res.send({ success: true, message: 'ToS accepted' });
        });
      });
  })
}