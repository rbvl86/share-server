'use strict';

var Action = require('../_models/action');
var Folder = require('../_models/folder');

var multer = require('multer');
var upload = multer({ dest: 'public/img/a' });

var authMw = require('../_mw/auth');

function getFolderCount(actionId) {
  return Folder.find({ actions: actionId }).count(function(error, count) {
    if (error) { return error; }

    return count;
  });
}

module.exports = function (app, router) {
  module.actions = {};

  router.get('/actions', authMw.checkJWT, function (req, res) {
    var query = Action.find({ draft: { $in: [false, '', null, undefined] } });
    if (req.query.query) {
      var words = req.query.query.replace('%20', '|');
      var regexp = new RegExp(words, 'i');
      query.or([
        { title: regexp },
        { description: regexp },
        { steps: regexp },
        { stakes: regexp },
        { objective: regexp },
        { rules: regexp },
        { sources: regexp }
      ]);
    }

    query
      .lean()
      .populate({
        path: 'author',
        model: 'User',
        populate: {
          path: 'partner',
          model: 'Partner'
        }
      })
      .populate('theme')
      .exec(function (error, actions) {
        if (error) return res.send(error);
        var counts = [];
        module.actions = actions;
        
        for (var i = 0; i < actions.length; i++) {
          counts[i] = getFolderCount(actions[i]._id);
        }

        Promise.all(counts).then(function(counts) {
          for (var i = 0; i < module.actions.length; i++) {
            module.actions[i].folderCount = counts[i];
          }

          res.send(module.actions);
        });
      });
  });

  router.post('/actions', authMw.checkJWT, upload.any(), function (req, res) {
    console.log(req.body);
    var action = new Action({
      title: req.body.title,
      theme: req.body.theme,
      actionType: req.body.actionType,
      objective: req.body.objective,
      description: req.body.description,
      steps: req.body.steps,
      gains: req.body.gains,
      stakes: req.body.stakes,
      effects: JSON.parse(req.body.effects),
      iso26kfoa: req.body.iso26kfoa,
      industry: req.body.industry,
      draft: req.body.draft,
      image: req.files[0] && '/public/img/a/'+req.files[0].filename,
      imageCredits: req.body.imageCredits,
      author: req.decoded._id
    });

    req.body.sources && (action.sources = JSON.parse(req.body.sources));
    req.body.rules && (action.rules = JSON.parse(req.body.rules));


    action.save(function (error) {
      if (error) { return res.status(555).send(error); }

      return res.send({ success: true, message: "Action has been saved" });
    });
  }),

  router.get('/actions/:id', authMw.checkJWT, function (req, res) {
    Action.findOne({ _id: req.params.id })
      .lean()
      .populate({
        path: 'author',
        model: 'User',
        populate: {
          path: 'partner',
          model: 'Partner'
        }
      })
      .populate('theme')
      .populate('industry')
      .populate('iso26kfoa')
      .populate('effects')
      .exec(function (error, action) {
        if (error) { return res.send(error); }

        Folder.find({ actions: req.params.id }).count(function(error, count) {
          if (error) { res.send(error) }

          action.folderCount = count;
          res.send(action);
        });
      });
  });

  router.get('/actions/:id/suggestions', authMw.checkJWT, function(req, res) {
    Action.findOne({ _id: req.params.id }, function(error, action) {
      if (error) { return res.status(555).send(error); }

      var q = Action.find({
        theme: action.theme,
        draft: { $in: [false, '', null, undefined] }
      })
      .where('_id').ne(req.params.id)
      .populate('theme')
      .populate({
        path: 'author',
        populate: 'partner'
      })
      .limit(3);

      q.exec(function(error, actions) {
        if (error) return res.status(555).send(error);
        return res.send(actions);
      });
    });
  });

  router.put('/actions/:id', authMw.checkJWT, upload.any(), function (req, res) {
    var query = { _id: req.params.id };
    var action = {};

    req.body.title && (action.title = req.body.title);
    req.body.theme && (action.theme = req.body.theme);
    req.body.objective && (action.objective = req.body.objective);
    action.steps = req.body.steps;
    action.description = req.body.description;
    action.stakes = req.body.stakes;
    action.gains = req.body.gains;
    req.body.effects && (action.effects = JSON.parse(req.body.effects));
    req.body.iso26kfoa && (action.iso26kfoa = req.body.iso26kfoa);
    req.body.imageCredits && (action.imageCredits = req.body.imageCredits);
    req.body.sources && (action.sources = JSON.parse(req.body.sources));
    req.body.rules && (action.rules = JSON.parse(req.body.rules));
    req.body.draft && (action.draft = req.body.draft);

    if (req.files) {
      req.files.length && (action.image = 'public/img/a/'+req.files[0].filename);
    }

    action.lastEdited = Date.now();

    Action.findOneAndUpdate(query, action, { new: true, runValidators: false }, function(error, action) {
      if (error) return res.status(555).send(error);

      return res.send('Action saved');
    });
  });

  router.delete('/actions/:id', authMw.checkJWT, function(req, res) {
    Action.findOne({ _id: req.params.id })
      .exec(function (error, action) {
        console.log(action);
        console.log(req.decoded);
        if (error) { return res.send(error); }

        if (!action) { return res.status(555).send({ success: false, message: 'Action not found' }); }

        if (action.author == req.decoded._id || req.decoded.role === 'ADMIN') {
          action.remove(function(error) {
            if (error) { return res.send(error); }

            return res.send({ success: true, message: 'Action has been removed' });
          });
        } else {
          return res.status(403).send({ success: false, message: 'You do not have the right to do this' });
        }
      });
  });
}