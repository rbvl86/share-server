'use strict';

var Folder = require('../_models/folder');

var authMw = require('../_mw/auth');

module.exports = function(app, router) {
  router.get('/folders/:id', authMw.checkJWT, function(req, res) {
    Folder.findOne({ _id: req.params.id })
      .lean()
      .populate({
        path: 'actions',
        model: 'Action',
        populate: {
          path: 'author',
          model: 'User',
          populate: {
            path: 'partner',
            model: 'Partner'
          }
        }
      })
      .populate({
        path: 'actions',
        model: 'Action',
        populate: {
          path: 'theme',
          model: 'Theme'
        }
      })
      .exec(function(error, folder) {
      if (error) { return res.send(error); }
      if (!folder) { return res.status(555).send({ success: false, message: 'Folder not found' }); }

      if (req.decoded._id == folder.owner || folder.users.indexOf(req.decoded._id) > -1 || req.decoded.role === 'ADMIN') {
        return res.send(folder);
      } else {
        return res.status(556).send({ success: false, message: 'You do not have the right to do this' });
      }
      });
  });

  router.post('/folders', authMw.checkJWT, function(req, res) {
    var folder = new Folder({
      name: req.body.name,
      owner: req.decoded._id
    });

    if (req.body.actions) {
      folder.actions = req.body.actions;
    }
    if (req.body.users) {
      folder.users = req.body.users;
    }
    if (req.body._id) {
      folder._id = req.body._id;
    }

    folder.save(function(error) {
      if (error) { return res.send({ success: false, message: error }) }

      res.send({ success: true, message: 'Folder created', folder: folder });
    });
  });

  router.put('/folders/:id', authMw.checkJWT, function(req, res) {
    Folder.findOne({ _id: req.params.id })
      .exec(function(error, folder) {
      if (error) { return res.status(555).send(error); }

      if (folder.owner == req.decoded._id || folder.users.indexOf(req.decoded._id) > -1 || req.decoded.role === 'ADMIN') {
        req.body.users && (folder.users = req.body.users);

        if (req.body.actions) {
          for (var i = 0; i < req.body.actions.length; i++) {
            folder.actions.push(req.body.actions[i]);
          }
        }

        req.body.name && (folder.name = req.body.name);

        folder.save(function(error) {
          if (error) { return res.status(555).send(error) }

          res.send({ success: true, message: 'Folder has been saved', folder: folder });
        });
      } else {
        return res.status(555).send({ success: false, message: 'You do not have the right to do this' });
      }
    });
  });

  router.delete('/folders/:id/delete_action/:actionId', authMw.checkJWT, function(req, res) {
    Folder.findById(req.params.id)
      .exec(function(error, folder) {
        if (error) return res.status(555).send(error);

        if (folder.owner == req.decoded._id || folder.users.indexOf(req.decoded._id) > -1 || req.decoded.role === 'ADMIN') {
          folder.actions = folder.actions.filter(function(x) { return x != req.params.actionId });

          folder.save(function(error, folder) {
            if (error) return res.status(555).send(error);

            return res.send(folder);
          })
        } else {
          return res.status(555).send({ success: false, message: 'You do not have sufficient rights to do this' });
        }
      });
  })

  router.delete('/folders/:id', authMw.checkJWT, function(req, res) {
    Folder.findOneAndRemove({ _id: req.params.id }, function(error, folder) {
      if (error) { return res.send(error); }

      return res.send(folder);
    });
  });
}