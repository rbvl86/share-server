'use strict';

var Action = require('../_models/action');
var EntrepriseAction = require('../_models/entreprise_action');
var ObjectId = require('mongoose').Types.ObjectId;

var authMw = require('../_mw/auth');
var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty();
const fs = require('fs');
const readline = require('readline');
var xlsx = require('node-xlsx').default;
var util = require('util');

module.i = 3;
module.importedAction = null;
module.output = [];

module.exports = function (app, router) {
  router.post('/import', authMw.checkJWT, multipartyMiddleware, function (req, res) {
    var file = req.files.file;
    // get workbook
    var excel = xlsx.parse(file.path);
    // then data inside first worksheet
    var ws = excel[0].data;

    /**
     * Current data structure:
     * [ 
     * 0 title: String, maxlength 140 required,
     * 1 objective: String, maxlength 140 required,
     * 2 stakes: String, maxlength 1400 required,
     * 3 description: String, maxlength 1400,
     * 4 steps: String, maxlength 1400 required,
     * 5 6 7 8 9 rules: Array [{ title: String, url: String, date: Date, comment: String }], maxlength 5,
     * 10 gains: String, maxlength 1400,
     * 11 theme: ObjectId, required,
     * 12 effects: Array [ ObjectId ], maxlength 5 required,
     * 13 industry: ObjectId, required,
     * 14 actionType: String, required,
     * 15 is26kfoa: ObjectId,
     * 16 standards: String,
     * 17 18 19 20 21 sources: Array [{ title: String, url: String }], maxlength 5,
     * 22 author: ObjectId,
     * 23 date: Date, defaults to now
     * 24 image: String, required,
     * 25 imageCredits: String,
     * 26 (ozactId): reference to Ozact Enterprise action if exists
     * ]
     */

    function _getRule(cell) {
      if (!cell) return false;

      var splittedRule = cell.split(';');
      var parsedRule = {};

      if (splittedRule.length < 3) { return false; }

      splittedRule[0] && (parsedRule.title = splittedRule[0]);
      splittedRule[1] && (parsedRule.url = splittedRule[1]);
      splittedRule[2] && (parsedRule.date = new Date(splittedRule[2]));
      splittedRule[3] && (parsedRule.comment = splittedRule[3]);

      return parsedRule;
    }

    function _getSource(cell) {
      if (!cell) return false;

      var splittedSource = cell.split(';');
      var parsedSource = {};

      if (splittedSource.length < 2) { return false; }

      parsedSource.title = splittedSource[0];
      parsedSource.url = splittedSource[1];

      return parsedSource;
    }

    function getImgUrl(cells) {


      if (cells[1]) {
        if (ObjectId.isValid(cells[1])) {
          return EntrepriseAction.findOne({ _id: cells[1] }).lean().exec();
        } else {
          return new Promise(function(done, error) {
            done(null);
          })
        }
      }

      return new Promise(function(done, error) {
        done('/public/img/a/'+cells[0]);
      });
    }

    function computeDataRow(i) {
      if (ws[i] && ws[i].length) {
        var rulesArray = [], sourcesArray = [];
        var effects, imgUrl, industry;

        ws[i][12] && (effects = ws[i][12].split(';'));
        if (ObjectId.isValid(ws[i][13])) { industry = ws[i][13]; }

        for (var j=5;j<=9;j++) {
          var rule = _getRule(ws[i][j]);
          rule && rulesArray.push(rule);
        }
        for (var j=17;j<=21;j++) {
          var source = _getSource(ws[i][j]);
          source && sourcesArray.push(source);
        }

        module.importedAction = new Action({
          title: ws[i][0],
          objective: ws[i][1],
          stakes: ws[i][2],
          description: ws[i][3],
          steps: ws[i][4],
          rules: rulesArray,
          gains: ws[i][10],
          theme: ws[i][11],
          effects: effects,
          industry: industry,
          actionType: ws[i][14],
          iso26kfoa: ObjectId.isValid(ws[i][15]) && ws[i][15],
          standards: ws[i][16],
          sources: sourcesArray,
          author: ObjectId.isValid(ws[i][15]) &&  ws[i][22],
          createdAt: new Date(ws[i][23]),
          imageCredits: ws[i][25],
        });

        console.log('generating image url for line '+parseInt(i+1));
        getImgUrl([ ws[i][24], ws[i][26] ]).then(function(url) {
          module.importedAction.image = url;
          if (url) {
            if (url.assets) {
              module.importedAction.image = 'https://greenlantern-cofely.s3.amazonaws.com/assets/assets/'+url.assets[0]._id+'/large.jpg';
              //also get date
              module.importedAction.createdAt = url.created_at;
              module.importedAction.entrepriseAction = url._id;
            }
          } else {
            console.log('----- WARNING, could not generate image url here, the Action will have no image -----');
          }

          Action.create(module.importedAction, function(error, action) {
          if (error) {
            module.output.push({ message: 'Could not import action', error: error });
          } else {
            module.output.push({ message: 'Action created', action: action });
          }
          
          module.i++;
          computeDataRow(module.i);
        })
        }).catch(function(error) { console.log(error); });
      } else {
        fs.unlink(req.files.file.path);
        return res.send(module.output);
      }
    }

    computeDataRow(module.i);
  });
}