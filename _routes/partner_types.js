'use strict';

var PartnerType = require('../_models/partner_type');

module.exports = function(app, router) {
  router.get('/partner_types', function(req, res) {
    PartnerType.find({})
      .exec(function(error, partner_types) {
        if (error) return res.status(555).send(error);

        return res.send(partner_types);
      });
  })
};