'use strict';

var User = require('../_models/user');
var Partner = require('../_models/partner');
var authMw = require('../_mw/auth');
var appConfig = require('../config/app.config');

var transporter = require('../nodemailer.config');

var multer = require('multer');

module.exports = function(app, router) {
  router.post('/admin/create_user', authMw.checkJWT, authMw.checkAdmin, multer({ dest: 'public/img/u' }).any(), function(req, res) {
    User.findOne({
      email: req.body.email
    }, function(error, user) {
      if (user) {
        return res.status(555).send({ success: false, message: 'Error while trying to create user. A user with this email address already exists' });
      }
    });

    var newUser = new User({
      email: req.body.email,
      password: req.body.password,
      firstname: req.body.firstname,
      lastname: req.body.lastname,
      bio: req.body.bio,
      partner: req.body.partner,
      role: req.body.role,
      title: req.body.title && req.body.title,
      avatar: req.files[0] && '/public/img/u/'+req.files[0].filename
    });

    newUser.save(function (error) {
      if (error) { return res.status(555).send({ message: 'User creation failed', error: error }) };

      var mailOptions = {
        from: '<rombevillard@gmail.com>',
        to: newUser.email,
        subject: 'Ozact Share - compte activé',
        html: 'Bonjour,<br><br>Félicitations ! Votre compte Ozact Share a bien été activé !<br>Nous vous remercions pour votre inscription.<br><br>L équipe Ozact Share'
      };

      if (appConfig.sendNewUserMail) {
        transporter.sendMail(mailOptions, function(error, info) {
          if (error) { console.log(error); } else { console.log(info.response) }
        });
      }

      return res.send({ success: true, message: 'User has been created', user: newUser });
    })
  });

  router.route('/admin/user/:id')
    .put(authMw.checkJWT, authMw.checkAdmin, function(req, res) {
      User.findById(req.params.id, function (error, user) {
        if (error) { return res.send(error); }
        
        if (!user) {
          return res.status(555).send({ success: false, message: 'Failed to update user. User with id '+ req.params.id +' not found' });
        }

        req.body.firstname && (user.firstname = req.body.firstname);
        req.body.lastname && (user.lastname = req.body.lastname);
        req.body.email && (user.email = req.body.email);
        req.body.partner && (user.partner = req.body.partner);
        req.body.role && (user.role = req.body.role);

        user.save(function (error) {
          if (error) { return res.send(error) }

          return res.send({ success: true, message: 'User saved' });
        });
      });
    })
    .delete(authMw.checkJWT, authMw.checkAdmin, function(req, res) {
      User.find({ id: req.params.id }).remove().exec(function (error) {
        if (error) { return res.send(error); }

        return res.send({ success: true, message: 'User removed successfully' });
      });
    });

  router.get('/admin/users', authMw.checkJWT, authMw.checkAdmin, function(req, res) {
    User.find({}).exec(function (error, users) {
      if (error) return res.send(error);

      return res.send(users);
    })
  });

  router.post('/admin/create_partner', authMw.checkJWT, authMw.checkAdmin, multer({ dest: 'public/img/p' }).any(), function(req, res) {
    var partner = new Partner({
      name: req.body.name,
      logo: req.files && '/public/img/p/'+req.files[0].filename,
      website: req.body.website,
      description: req.body.description,
      streetAddress: req.body.streetAddress,
      postcode: req.body.postcode,
      city: req.body.city
    });

    partner.save(function(error, partner) {
      if (error) { return res.status(555).send({ success: false, message: 'Error while creating partner', error: error }) }

      return res.send(partner);
    });
  });

  router.delete('/admin/partner/:id', authMw.checkJWT, authMw.checkAdmin, function(req, res) {
    Partner.findOneAndRemove({ _id: req.params.id }, function(error) {
      if (error) { return res.status(555).send({ success: false, message: 'Error while deleting partner', error: error }) }

      return res.send({ success: true, message: 'Partner has been deleted' });
    });
  });
}