'use strict';

var Industry = require('../_models/industry');

module.exports = function(app, router) {
  router.get('/industries', function(req, res) {
    Industry.find({}).exec(function(error, industries) {
      if (error) { res.send(error); }

      res.send(industries);
    })
  });
}