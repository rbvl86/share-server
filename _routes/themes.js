'use strict';

var authMw = require('../_mw/auth');
var Theme = require('../_models/theme');
var Action = require('../_models/action');

module.exports = function (app, router) {
  router.get('/themes', function (req, res) {
    Theme.find().exec(function (error, themes) {
      if (error) { return res.send(error); }

      res.send(themes);
    });
  });

  router.get('/themes/:id/actions', authMw.checkJWT, function (req, res) {
    Action.find({ theme: req.param.id })
      .exec(function(error, actions) {
        if (error) { return res.send(error); }

        res.send(actions);
      });
  });
};