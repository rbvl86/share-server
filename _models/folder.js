'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var FolderSchema = new Schema({
  name: { type: String, required: true },
  actions: [{ type: ObjectId, ref: 'Action', default: [] }],
  owner: { type: ObjectId, ref: 'User', required: true },
  users: [{ type: ObjectId, ref: 'User', default: [] }]
});

module.exports = mongoose.model('Folder', FolderSchema);
