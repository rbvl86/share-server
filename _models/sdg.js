'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SdgSchema = new Schema({
  name: { type: String },
  description: { type: String }
});

module.exports = mongoose.model('Sdg', SdgSchema);