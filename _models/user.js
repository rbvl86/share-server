'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var bcrypt = require('bcrypt');

var UserSchema = new Schema({
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  firstname: { type: String, required: true },
  title: { type: String, default: '', maxlength: 60 },
  lastname: { type: String, required: true },
  bio: { type: String, default: '' },
  lastActive: { type: Date, default: Date.now() },
  partner: { type: ObjectId, ref: 'Partner' },
  role: { type: String, enum: [ 'ADMIN', 'KM', 'USER' ], default: 'USER' },
  linkedin: { type: String, default: '' },
  avatar: { type: String, default: '/assets/img/avatar.png' },
  createdAt: { type: String, default: Date.now() },
  accepted_tos: { type: Boolean, default: false }
});

UserSchema.pre('save', function(next) {
  var user = this;

  if(user.isModified('password') || user.isNew) {
    bcrypt.genSalt(10, function(error, salt) {
      if (error) {
        console.log(error);
        return next(error);
      }

      bcrypt.hash(user.password, salt, function (error, hash) {
        if (error) {
          console.log(error);
          return next(error);
        }
        user.password = hash;
        next();
      });
    })
  } else {
    return next();
  }
});

UserSchema.methods.comparePassword = function(passw, done) {
  var user = this;
  bcrypt.compare(passw, user.password, function(error, isMatch) {
    if (error) {
      return done(error);
    }
    done(false, isMatch);
  });
};

module.exports = mongoose.model('User', UserSchema);
