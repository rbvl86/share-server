'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PartnerTypeSchema = new Schema({
  name: { type: String, default: '' },
  name_many: { type: String, default: '' },
  description: { type: String, default: '' }
});

module.exports = mongoose.model('PartnerType', PartnerTypeSchema, 'partner_types');