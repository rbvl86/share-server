var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Iso26kcentral = new Schema({
  name: { type: String, required: true }
});

module.exports = mongoose.Model('Iso26kcentral', Iso26kcentral);