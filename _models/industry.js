'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var IndustrySchema = new Schema({
  name: { type: String },
  description: { type: String }
});

module.exports = mongoose.model('Industry', IndustrySchema);
