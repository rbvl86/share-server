'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var PartnerSchema = new Schema({
  name: { type: String, required: true },
  logo: { type: String },
  website: { type: String, default: '' },
  description: { type: String, default: '' },
  streetAddress: { type: String, default: '' },
  postcode: { type: String, default: '' },
  city: { type: String, default: '' },
  partner_type_ids: [{ type: ObjectId, ref: 'PartnerType' }],
  createdAt: { type: Date, default: Date.now() }
});

module.exports = mongoose.model('Partner', PartnerSchema);
