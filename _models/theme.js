'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ThemeSchema = new Schema({
  name: { type: String },
  color: { type: String },
  image: { type: String },
  description: { type: String }
});

module.exports = mongoose.model('Theme', ThemeSchema);
