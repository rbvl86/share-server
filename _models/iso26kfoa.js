var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var Iso26kfoa = new Schema({
  name: { type: String, required: true },
  iso26kcentral: { type: ObjectId, ref: 'Iso26kcentral', required: true }
});

module.exports = mongoose.model('Iso26kfoa', Iso26kfoa, 'iso26kfoa');