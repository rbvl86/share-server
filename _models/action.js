'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var ActionSchema = new Schema({
  title: { type: String, maxlength: 140, required: true },
  objective: { type: String, maxlength: 140 },
  stakes: { type: String, default: '' },
  description: { type: String, default: '' },
  steps: { type: String, default: '' },
  rules: [{}],
  gains: { type: String },
  theme: { type: ObjectId, ref: 'Theme', required: true },
  actionType: { type: String, enum: [ 'DIAG', 'TRAIN', 'COM', 'EQUIP', 'PROC', 'ECO' ], required: true },
  iso26kfoa: { type: ObjectId, ref: 'Iso26kfoa' },
  standards: { type: String, default: '' },
  sources: [{}],
  draft: { type: Boolean, default: false },
  author: { type: ObjectId, ref: 'User' },
  createdAt: { type: Date, default: Date.now() },
  image: { type: String, default: '' },
  imageCredits: { type: String, default: '' },
  effects: [{ type: ObjectId, ref: "Effect" }],
  industry: { type: ObjectId, ref: 'Industry' },
  publishedAt: { type: Date, default: Date.now() },
  lastEdited: { type: Date, default: Date.now() },
  entrepriseAction: { type: ObjectId, ref: 'EntrepriseAction', default: null }
});

module.exports = mongoose.model('Action', ActionSchema);
