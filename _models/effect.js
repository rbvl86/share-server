'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var EffectSchema = new Schema({
  name: { type: String, required: true },
  image: { type: String, required: true },
  themes: [{ type: ObjectId, ref: 'Theme' }],
  description: { type: String, default: '' }
});

module.exports = mongoose.model('Effect', EffectSchema);
