module.exports = new Sequelize('ozactShare', 'root', '', {
    host: "127.0.0.1",
    port: 8889,
    dialect: 'mysql',
    dialectOptions: {
        socketPath: '/var/run/mysqld/mysqld.sock'
    },
    define: {
        timestamps: false,
        freezeTableName: true
    },
    omitNull: true
}); 